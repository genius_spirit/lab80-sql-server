const express = require('express');
const multer = require('multer');
const path = require('path');
const nanoid = require('nanoid');

const config = require('../config');
const router = express.Router();

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  }
});

const upload = multer({storage});

const createRouter = (db) => {

  router.get('/', (req, res) => {
    db.query('SELECT * FROM `items`', function (error, results) {
      if (error) throw error;
      res.send(results);
    })
  });

  router.get('/:id', (req, res) => {
    const id = req.params.id;
    db.query('SELECT * FROM `items` WHERE `id`= "'+ id + '"', (error, results) => {
      if (error) throw error;
      res.send(results);
    })
  });

  router.post('/', upload.single('photo'), (req, res) => {
    const data = req.body;
    if (req.file) {
        data.photo = req.file.filename;
      } else {
        data.photo = null;
      }

    if (data.name && data.categoryId && data.locationId) {
      db.query('INSERT INTO `items` (`name`, `category_id`, `location_id`, `description`, `photo`) VALUES (?, ?, ?, ?, ?)',
        [data.name, data.categoryId, data.locationId, data.description, data.photo],
        (error, results) => {
          if (error) throw error;
          data.id = results.insertId;
          res.send(data);
        }
      )
    } else res.status(400).send('Fields "Name", "category_id", "location_id" can not be blank');
  });

  router.delete('/:id', (req, res) => {
    const id = req.params.id;

    db.query('DELETE FROM `items` WHERE `id`= "'+ id + '"', (error, results) => {
      if (error) throw error;
      data.id = id;
      res.send(data);
    });
  });

  router.put('/:id', upload.single('photo'), (req, res) => {
    const data = req.body;
    if (req.file) {
      data.photo = req.file.filename;
    } else {
      data.photo = null;
    }

    const id = req.params.id;

    let photo = null;

    if (data.name && data.categoryId && data.locationId) {
      const params = [data.name, data.categoryId, data.locationId, data.description];

      if (data.photo) {
        photo = '`,photo` = ?';
        params.push(data.photo);
      } else {
        photo = '';
      }

      params.push(id);
      console.log(params);

      db.query('UPDATE `items` SET `name` = ?, `category_id` = ?, `location_id` = ?, `description` = ? ' + photo + ' WHERE `id` = ?', params,
        (error, results) => {
          if (error) throw error;
            data.id = id;
            res.send(data);
      });
    }
  });

  return router;
};

module.exports = createRouter;